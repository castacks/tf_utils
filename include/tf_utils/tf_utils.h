/* Copyright 2014 Sanjiban Choudhury
 * tf_utils.h
 *
 *  Created on: May 21, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef INCLUDE_TF_UTILS_TF_UTILS_H_
#define INCLUDE_TF_UTILS_TF_UTILS_H_

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <visualization_msgs/InteractiveMarker.h>
#include <Eigen/StdVector>

namespace ca {
namespace tf_utils {

/**
 * \brief Gets the transform between frames from the listener and saves it in the transform reference
 * @param listener The listener used to get the transfrom
 * @param fromFrame Parent frame
 * @param toFrame Child frame
 * @param waitTime Time in seconds to wait for the transform
 * @param transform Transform stored
 * @return true if transform was computed, false if not
 */
bool getCoordinateTransform(const tf::TransformListener &listener,
                            std::string fromFrame,
                            std::string toFrame,
                            double waitTime,
                            tf::StampedTransform &transform);

/**
 * \brief Gets the transform between frames from the listener and saves it in the transform reference
 * @param listener The listener used to get the transfrom
 * @param fromFrame Parent frame
 * @param toFrame Child frame
 * @param waitTime Time in seconds to wait for the transform
 * @param transform Transform stored
 * @return true if transform was computed, false if not
 */
bool getCoordinateTransform(const tf::TransformListener &listener,
                            std::string fromFrame,
                            std::string toFrame,
                            double waitTime,
                            tf::StampedTransform &transform, double queryTime);

/**
 * \brief Transforms a geometry_msgs quaternion
 * @param transform transform
 * @param quat_in quaternion to be transformed
 */
void transformQuaternion(const tf::Transform &transform,
                         geometry_msgs::Quaternion& quat_in);

/**
 * \brief Transforms a geometry_msgs Pose
 */
void transformPose(const tf::Transform &transform,
                   geometry_msgs::Pose& pose_in);

/**
 * \brief Transforms a geometry_msgs Point
 */
void transformPoint(const tf::Transform &transform,
                    geometry_msgs::Point& point_in);

/**
 * \brief Transforms an eigen Point represented through vector3d
 */
void transformPoint(const tf::Transform &transform,
                    Eigen::Vector3d& point_in);

/**
 * \brief Transforms a vector of points
 */
void transformPointList(const tf::Transform &transform,
                        std::vector<geometry_msgs::Point>& point_in);

/**
 * \brief Transforms a geometrymsgs Vector3
 * // Vectors should just be rotated as it is vector not a point, but for backwards compatibility the default does both rotation and translation
 */
void transformVector(const tf::Transform &transform_c,
                     geometry_msgs::Vector3& vector_in, bool vector_just_does_rotation = false);

/**
 * \brief Transforms all points of the marker
 */
void transformMarker(const tf::Transform &transform,
                     visualization_msgs::Marker &marker);

/**
 * \brief Transforms ONLY pose of odom
 */
void transformOdom(const tf::Transform &transform,
                   nav_msgs::Odometry &odom);

/**
 * \brief Transforms pose and linear twist of odom
 */
void transformOdom(const tf::Transform &transform_pose,
                   tf::Transform &transform_velocity, nav_msgs::Odometry &odom);

/**
 * \brief Transforms Eigen vector3d
 * // Vectors should just be rotated as it is vector not a point, but for backwards compatibility the default does both rotation and translation
 */
void transformVector3d(const tf::Transform &transform_c, Eigen::Vector3d &vec, bool vector_just_does_rotation = false);

/**
 * \brief Transforms Eigen vector3f
 * // Vectors should just be rotated as it is vector not a point, but for backwards compatibility the default does both rotation and translation
 */
void transformVector3f(const tf::Transform &transform_c, Eigen::Vector3f &vec, bool vector_just_does_rotation = false);

/**
 * \brief Transforms a STL vector (2d / 3d)
 * // Vectors should just be rotated as it is vector not a point, but for backwards compatibility the default does both rotation and translation
 */
void transformVector(const tf::Transform &transform_c,
                     std::vector<double> &vec, bool vector_just_does_rotation = false);

}  // namespace tf_utils
}  // namespace ca

#endif  // INCLUDE_TF_UTILS_TF_UTILS_H_
