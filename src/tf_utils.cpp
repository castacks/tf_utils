/*
* Copyright (c) 2016 Carnegie Mellon University, Author <sanjiban@cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


/* Copyright 2014 Sanjiban Choudhury
 * tf_utils.cpp
 *
 *  Created on: May 21, 2014
 *      Author: Sanjiban Choudhury
 */

#include <tf_conversions/tf_eigen.h>
#include "tf_utils/tf_utils.h"

namespace ca {
namespace tf_utils {

bool getCoordinateTransform(const tf::TransformListener &listener,
                            std::string fromFrame,
                            std::string toFrame,
                            double waitTime,
                            tf::StampedTransform &transform) {
  bool flag = true;
  double timeResolution = 0.001;
  unsigned int timeout = ceil(waitTime/timeResolution);
  unsigned int count = 0;

  while(flag) {
    flag = false;
    try {
      count++;
      listener.lookupTransform(toFrame,fromFrame,ros::Time(0), transform);
    }
    catch (tf::TransformException &ex) {
      flag = true;
      if(count>timeout) {
        flag = false;
        ROS_ERROR_STREAM("Cannot find transform from::"<<fromFrame << " to::"<< toFrame);
        return flag;
      }
      ros::Duration(timeResolution).sleep();
    }
  }
  return (!flag);
}

bool getCoordinateTransform(const tf::TransformListener &listener,
                            std::string fromFrame,
                            std::string toFrame,
                            double waitTime,
                            tf::StampedTransform &transform, double queryTime) {
  bool flag = true;
  double timeResolution = 0.001;
  unsigned int timeout = ceil(waitTime/timeResolution);
  unsigned int count = 0;

  while(flag) {
    flag = false;
    try {
      count++;
      listener.lookupTransform(toFrame,fromFrame,ros::Time(queryTime), transform);
    }
    catch (tf::TransformException &ex) {
      flag = true;
      if(count>timeout) {
        flag = false;
        ROS_ERROR_STREAM("Cannot find transform from::"<<fromFrame << " to::"<< toFrame);
        return flag;
      }
      ros::Duration(timeResolution).sleep();
    }
  }
  return (!flag);
}


void transformQuaternion(const tf::Transform &transform,
                         geometry_msgs::Quaternion& quat_in) {
  tf::Quaternion q,qOut;
  tf::quaternionMsgToTF(quat_in, q);
  qOut = transform*q;
  tf::quaternionTFToMsg(qOut,quat_in);
}

void transformPose(const tf::Transform &transform,
                   geometry_msgs::Pose& pose_in) {
  tf::Pose p,pOut;
  tf::poseMsgToTF(pose_in, p);
  pOut = transform*p;
  tf::poseTFToMsg(pOut,pose_in);
}

void transformPoint(const tf::Transform &transform,
                    geometry_msgs::Point& point_in) {
  tf::Point p,pOut;
  tf::pointMsgToTF(point_in, p);
  pOut = transform*p;
  tf::pointTFToMsg(pOut,point_in);
}

void transformPoint(const tf::Transform &transform,
                    Eigen::Vector3d& point_in){
    tf::Vector3 v;
  #if ROS_VERSION_MINIMUM(1, 9, 50) // groovy
    tf::vectorEigenToTF(point_in, v);
    tf::vectorTFToEigen(transform*v, point_in);
  #else
    tf::VectorEigenToTF(point_in, v);
    tf::VectorTFToEigen(transform*v, point_in);
  #endif
}

void transformPointList(const tf::Transform &transform,
                        std::vector<geometry_msgs::Point>& point_in) {
  for(size_t i=0; i< point_in.size(); i++)
    transformPoint(transform,point_in[i]);
}
// Vectors should just be rotated as it is vector not a point, but for backwards compatibility the default does both rotation and translation
void transformVector(const tf::Transform &transform_c,
                     geometry_msgs::Vector3& vector_in, bool vector_just_does_rotation) {
    tf::Transform transform = transform_c;
  if(vector_just_does_rotation){
        tf::Vector3 zeros(0.0,0.0,0.0);
        transform.setOrigin(zeros);
  }
  tf::Vector3 v,vOut;
  tf::vector3MsgToTF(vector_in, v);
  vOut = transform*v;
  tf::vector3TFToMsg(vOut,vector_in);
}

void transformMarker(const tf::Transform &transform,
                     visualization_msgs::Marker &marker) {
  transformPose(transform,marker.pose);
  for(size_t i=0; i<marker.points.size(); i++)
    transformPoint(transform,marker.points[i]);
}

void transformOdom(const tf::Transform &transform,
                   nav_msgs::Odometry &odom){
  transformPose(transform, odom.pose.pose);
  //ROS_WARN("This transformOdom function only transforms pose");
}

void transformOdom(const tf::Transform &transform_pose,
                   tf::Transform &transform_velocity, nav_msgs::Odometry &odom) {
  transformPose(transform_pose, odom.pose.pose);
  tf::Vector3 v(0,0,0);
  transform_velocity.setOrigin(v);
  transformVector(transform_velocity, odom.twist.twist.linear);
}

// Vectors should just be rotated as it is vector not a point, but for backwards compatibility the default does both rotation and translation
void transformVector3d(const tf::Transform &transform_c,
                       Eigen::Vector3d &vec, bool vector_just_does_rotation) {
  tf::Vector3 v;
  tf::Transform transform = transform_c;
  if(vector_just_does_rotation){
      tf::Vector3 zeros(0.0,0.0,0.0);
      transform.setOrigin(zeros);
  }
#if ROS_VERSION_MINIMUM(1, 9, 50) // groovy
  tf::vectorEigenToTF(vec, v);
  tf::vectorTFToEigen(transform*v, vec);
#else
  tf::VectorEigenToTF(vec, v);
  tf::VectorTFToEigen(transform*v, vec);
#endif
}

// Vectors should just be rotated as it is vector not a point, but for backwards compatibility the default does both rotation and translation
void transformVector3f(const tf::Transform &transform_c,
                       Eigen::Vector3f &vec, bool vector_just_does_rotation) {
  tf::Vector3 v;
  tf::Transform transform = transform_c;
  if(vector_just_does_rotation){
      tf::Vector3 zeros(0.0,0.0,0.0);
      transform.setOrigin(zeros);
  }
#if ROS_VERSION_MINIMUM(1, 9, 50) // groovy
  tf::vectorEigenToTF(vec.cast<double>(), v);
  Eigen::Vector3d vecd;
  tf::vectorTFToEigen(transform*v, vecd);
  vec = vecd.cast<float>();
#else
  tf::VectorEigenToTF(vec.cast<double>(), v);
  Eigen::Vector3d vecd;
  tf::VectorTFToEigen(transform*v, vecd);
  vec = vecd.cast<float>();
#endif
}
// Vectors should just be rotated as it is vector not a point, but for backwards compatibility the default does both rotation and translation
void transformVector(const tf::Transform &transform_c,
                     std::vector<double> &vec, bool vector_just_does_rotation) {
  tf::Vector3 v, vout;
  tf::Transform transform = transform_c;
  if(vector_just_does_rotation){
      tf::Vector3 zeros(0.0,0.0,0.0);
      transform.setOrigin(zeros);
  }
  if (vec.size() == 2) {
    tf::Vector3 v = transform*tf::Vector3(vec[0], vec[1], 0.0);
    vec[0] = v.getX(); vec[1] = v.getY();
  } else if (vec.size() == 3) {
    tf::Vector3 v = transform*tf::Vector3(vec[0], vec[1], vec[2]);
    vec[0] = v.getX(); vec[1] = v.getY(); vec[2] = v.getZ();
  }
}

}  // namespace tf_utils
}  // namespace ca

/*
 * void CA::TransformVector(tf::Transform transform,geometry_msgs::Vector3& vector_in)
{
  tf::Vector3 zeros(0.0,0.0,0.0);
  transform.setOrigin(zeros);
  tf::Vector3 v,v_out;
  tf::vector3MsgToTF(vector_in, v);
  transform.setOrigin(zeros);
  v_out = transform*v;
  tf::vector3TFToMsg(v_out,vector_in);
};
 */
